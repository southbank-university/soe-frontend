SOE.addModule('events', {

	_events: {},

	getListeners : function(event) {
		var listeners = this._events[event];
		if (!listeners)
			listeners = this._events[event] = [];
		return listeners;
	},

	listen: function(event, listener) {
		this.getListeners(event).push(listener);
	},

	trigger: function(event, data = null) {
		var listeners = this.getListeners(event);
		for (var i = 0; i < listeners.length; i++) {
			listeners[i](data);
		}
	}

})
