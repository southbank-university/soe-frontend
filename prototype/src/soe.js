const SOE = {

  _modules: {},

  addModule: function(name, module) {
    this._modules[name] = this[name] = module;
  },

  ready:function() {
    this.invoke('ready');
  },

  invoke:function(func) {
    for (var name in this._modules) {
      var module = this._modules[name];
      if (module[func]) module[func]();
    }
  }

};
