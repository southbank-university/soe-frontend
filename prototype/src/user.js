SOE.addModule('user', {
	_tempName: null,

	ready: function() {
		if (this.isLoggedIn()) {
			this.load();
		}
		this.checkHosts();
	},

	checkHosts: function() {
		var server = SOE.server;
		/*
	Check if the server login state and the client login state match.
	If they do not, we attempt to correct this for the client.
	*/
		//TODO: Untested behaviour when logged into mulitple at once.
		for (var i = 0; i < server.getHostLocations(); i++) {
			var host = server.getHostLocations()[i];
			this.checkHost(host);
		}
	},

	checkHost: function(host) {
		var server = SOE.server;
		var user = SOE.user;
		var self = this;
		//TODO: Refactor this stuff.
		server.asyncLoginStatus(host).then(function(data) {
			if (data != '1') {
				if (user.getActiveHost() == host && user.hasData()) {
					//TODO: Message to alert the User they were logged out of the server.
					user.logout();
				}
			} else if (!user.hasActiveHost()) {
				/*
		Server says we are logged in but we are missing data.
		Request data from server then goto profile.
	*/
				user.setActiveHost(host);
				user.load();
			}
		});
	},

	getActiveHost: function() {
		return localStorage.activeHost;
	},

	setActiveHost: function(host) {
		if (host == null) {
			localStorage.removeItem('activeHost');
			return;
		}
		localStorage.activeHost = host
	},

	hasActiveHost: function() {
		return this.getActiveHost() != null;
	},

	getData: function() {
		if (!localStorage.userData)
			return null;
		try {
			return JSON.parse(localStorage.userData)
		} catch (e) {
			console.error(e);
		}
		return null;
	},

	hasData: function() {
		return this.getData() != null;
	},

	setData: function(data) {
		if (data == null) {
			localStorage.removeItem('userData');
			return;
		}
		localStorage.userData = JSON.stringify(data);
	},

	getName: function() {
		if (this._tempName != null)
			return this._tempName;
		return this.hasData()
			? this.getData().name
			: null;
	},

	logout: function(host = null) {
		var self = this;
		//TODO: For whatever reason logout ALWAYS gives us a 403, even if it does log out the User. So as a hack we just always assume that they are logged out.
		//This is a hack, will always return 403, but we ignore because the User is actually logged out.
		SOE.server.requestGetLogout(host == null
			? self.getActiveHost()
			: host).then(onLogout, onLogout);
		function onLogout() {
			self.reset();
			SOE.events.trigger('onLogoutDone');
		}
	},

	login: function(host, username, password) {
		var self = this;
		SOE.server.requestPostLogin(host, username, password).then(function(data) {
			self._tempName = data['current_user'].name;
			onLogin();
		}, function(error) {
			if (error.status) {
				//If we already logged in on this server, a 403 is returned, probs not the best way of checking.
				//TODO: Will lead to undefined behaviour.
				if (error.status == 403) {
					self.setActiveHost(host);
					self.load();
					onLogin();
					return;
				}
			}
			console.log(error);
			SOE.events.trigger('onLoginFail', error);
		});

		function onLogin() {
			self.setActiveHost(host);
			SOE.events.trigger('onLoginDone');
			self.load();
		}
	},

	load: function() {
		var self = this;
		if (self.hasData()) {
			SOE.events.trigger('onUserLoaded');
		} else {
			SOE.server.requestGetUserData(self.getActiveHost()).then(function(data) {
				self.setData(self.sanitizeData(data));
				SOE.events.trigger('onUserLoaded');
			}).catch(function(error) {
				console.error(error);
			})
		}
	},

	isLoggedIn: function() {
		return this.hasActiveHost();
	},

	sanitizeData: function(raw) {
		var data = {};
		for (var key in raw) {
			var obj = raw[key][0];
			if (obj)
				data[key] = obj.value;
			}
		return data;
	},

	reset: function() {
		this.setData(null);
		this.setActiveHost(null);
		SOE.content.clearCache();
	}
})
