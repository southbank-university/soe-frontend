SOE.addModule('server', {

	//TODO: Un-hardcode hosts.
	//XXX: BAD IDEA to put this in the source code but Admin/Password will login to test remote.
	hosts : {
		'Test Remote': 'https://dev-censkhsoe.pantheonsite.io/',
		'Local': 'http://localhost/drupal/'
	},

	/**
	Have a feeling all requests to the server should be seperated from the
	other files and move to here.
**/

	apis : [
		'travel', 'music', 'sport', 'twitter'
	],

	getHostLocations : function() {
		var self = this;
		var locations = [];
		Object.keys(this.hosts).map(function(name) {
			locations.push(self.hosts[name])
		});
		return locations;
	},

	getHostLocation : function(index) {
		return this.hosts[Object.keys(this.hosts)[index]];
	},

	requestPostLogin : function(host, username, password) {
		var user = SOE.user;
		return ajaxPromise({
			url: host + 'user/login?_format=json',
			method: 'POST',
			headers: {
				"Content-type": "application/json"
			},
			data: JSON.stringify({"name": username, "pass": password})
		});
	},

	requestGetUserData: function(host) {
		return ajaxPromise({
			url: host + 'user/1?_format=json',
			method: 'GET'});
	},

	requestGetLogout: function(host) {
		return ajaxPromise({
			url: host + 'user/logout',
			method: 'GET',
			headers: {
				'Content-type': 'text/html'
			}
		})
	},

	requestGetCategoryData:function(host, category) {
		return ajaxPromise({
			url: host + 'api/' + category + "?_format=json",
			method: 'GET',
			headers: {
				'Content-type': "application/json"
			}
		});
	},

	requestGetLoginStatus : function(host = null) {
		return ajaxPromise({
			url: (host
				? host
				: SOE.user.getActiveHost()) + 'user/login_status?_format=json',
			headers: {
				'Content-type': 'application/json'
			},
			method: 'GET'
		});
	}
})
