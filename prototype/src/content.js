SOE.addModule('content', {

	_categoryCache: {},
	_tabs: [],
	_selectedTab: null,

	ready: function() {
		this.setupTabs();
	},

	clearCache: function() {
		this._categoryCache = {};
	},

	asyncDisplayCategory: function(category) {
		var self = this;
		var events = SOE.events;
		events.trigger('onContentLoadBegin', category);
		return this.asyncRequestCategory(category).then(function(data) {
			events.trigger('onContentLoadDone', {
				category: category,
				data: data
			});
		}, function(error) {
			events.trigger('onContentLoadFail', error);
			console.error(error);
		});
	},

	asyncRequestCategory: function(category) {
		var self = this;
		return new Promise(function(resolve, reject) {
			//Check for cached copy, otherwise request from active host.
			var cached = self._categoryCache[category];
			if (cached == null) {
				SOE.server.requestGetCategoryData(SOE.user.getActiveHost(), category).then(function(data) {
					resolve(self._categoryCache[category] = data);
				}, function(error) {
					reject(error);
				});
			} else {
				resolve(cached);
			}
		})
	},

	asyncDisplayDefault: function() {
		return this.asyncDisplayCategory(SOE.server.apis[0]);
	},

	getTab: function(category) {
		return this._tabs[category];
	},

	setupTabs: function() {
		var server = SOE.server;
		var self = this;
		for (var i = 0; i < server.apis.length; i++) {
			const api = server.apis[i];
			SOE.events.trigger('onContentTabAdded', api);

		}
	},

	selectTab: function(name) {
		if (!this.isTabSelected(name)) {
			this.setSelectedTab(name);
			this.asyncDisplayCategory(name);
		}
	},

	getSelectedTab: function() {
		return this._selectedTab;
	},

	setSelectedTab: function(tab) {
		this._selectedTab = tab;
		SOE.events.trigger('onContentTabSelected', tab);
	},

	isTabSelected: function(name) {
		return this._selectedTab == name;
	}
})
