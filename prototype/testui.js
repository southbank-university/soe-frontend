//TODO: although only for testing purposes, could use some cleanup.
var testUi = {

	_contentTabElements: {},

	ready: function() {
		this.setupLoginButton();
		this.setupLogoutButton();
		this.setupHostOptions();
		this.registerEvents();
	},

	registerEvents: function() {
		var self = this;
		var events = SOE.events;

		events.listen('onLoginDone', function() {
			self.toggleLoginButton(true);
			self.hideInfo();
		});

		events.listen('onUserLoaded', function() {
			self.gotoProfile(true);
		})

		events.listen('onLoginFail', function(error) {
			self.toggleLoginButton(true);
			if (error.status === 400)
				self.displayInfo(error.responseJSON.message);
			else {
				//Server says we are logged in so we ask to log out.
				self.toggleLoginButton(false);
				self.displayInfo('There was an error logging you, please try again.')
			}
		})

		events.listen('onUserLoaded', function() {
			SOE.content.asyncDisplayDefault();
			self.updateDisplayUserInfo();
		});

		events.listen('onLogoutDone', function() {
			$('.logout').attr('disabled', false);
			$('.profile').fadeOut()
			$('.login-area').slideDown();
			$('.content-container').fadeOut();
		});

		events.listen('onContentTabAdded', function(name){
			var content = SOE.content;
			const tab = $('<button class="content-tab">' + name + '</button>');
			tab.click(function() {
				content.selectTab(name);
			});
			testUi._contentTabElements[name] = tab;
			$('.content-tabs').append(tab);
		})

		events.listen('onContentTabSelected', this.selectTab)

		events.listen('onContentLoadDone', this.onContentLoadDone);
		events.listen('onContentLoadFail', this.onContentLoadFail);
		events.listen('onContentLoadBegin', this.onContentLoadBegin);
	},

	selectTab: function(name) {
			$('.content-tab.selected').toggleClass('selected', false);
			testUi._contentTabElements[name].toggleClass('selected', true);
	},

	onContentLoadBegin: function(category) {
		testUi.displayInfo('Loading content for ' + category + '...');
	},

	onContentLoadFail: function(category,error) {
		testUi.displayInfo('Error loading content :(');
	},

	onContentLoadDone: function(e) {
		var category = e.category;
		var data = e.data;

		testUi.hideInfo();

		testUi.clearContent();
		testUi.selectTab(category);

		//Updates display
		$('.content-container').fadeIn(200);
		$('.content').hide().fadeIn(200);

		if (data.length == 0) {
			$('.content').text("No content found for " + category + ".");
		} else {
			for (var i = 0; i < data.length; i++) {
				$('.content').append(testUi.createContentCard(data[i]));
			}
		}
	},

	clearContent: function() {
		$('.content').empty();
	},

	createContentCard: function(data) {
		var title = data.title;
		var body = data.body;
		var image = $(data.image);
		image.attr('src', SOE.user.getActiveHost() + image.attr('src'));
		return $('<div></div>').toggleClass('content-card').append($('<p>' + title + '</p>')).append(image).append($('<p>' + body + '</p>'))

	},

	clearDisplay: function() {
		this.getContainer().empty();
	},

	setupLoginButton: function() {
		var self = this;
		var user = SOE.user;

		$('.login-area').submit(function() {

			var fields = self.getLoginFields();
			var host = fields.host;

			self.displayInfo('Logging you in...');
			self.toggleLoginButton(false);

			user.login(host, fields.username, fields.password);
			return false;
		});

	},

	gotoProfile: function(animate) {
		this.clearContent();
		var profileDiv = $('.profile');
		var loginArea = $('.login-area');
		if (animate) {
			profileDiv.hide().fadeIn();
			loginArea.slideUp();
		} else {
			profileDiv.show();
			loginArea.hide();
		}
	},

	setupLogoutButton: function() {
		var user = SOE.user;

		$('.logout').click(function() {
			$('.logout').attr('disabled', true)
			user.logout()
		});

	},

	setupHostOptions: function() {
		var server = SOE.server;
		var selectionElement = $('.host-selection');
		for (var name in server.hosts) {
			var host = server.hosts[name];
			selectionElement.append($('<option></option>').text(name + '  (' + host + ')').attr('value', host));
		}
	},

	getLoginFields: function() {
		return {host: $('.host-selection').val(), username: $(".username").val(), password: $(".password").val()};
	},

	toggleLoginButton: function(state) {
		$('.login-area button').attr('disabled', !state);
	},

	updateDisplayUserInfo: function() {
		var user = SOE.user;
		$('.user-name').text(user.getName());
		$('.host').text(user.getActiveHost());
	},

	displayInfo: function(msg) {
		var info = $('.info').hide().html(msg);
		if (msg != null)
			info.fadeIn();
		}
	,

	hideInfo: function() {
		this.displayInfo(null);
	}
}
