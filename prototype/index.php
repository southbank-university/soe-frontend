<!DOCTYPE html>
<html>

	<?php
		$host = isset($_GET['host']) ? $_GET['host'] : null;
	?>

	<head>
		<title>Frontend Prototype</title>

		<link rel="stylesheet" href="main.css"/>

		<script src="lib/jquery-3.1.1.min.js"></script>

		<!-- Nicer way of importing would be great, somthing akin to node's require() or possibly a gulp file to concat and minify -->
		<?php
			$js_imports = ['soe','events','util','content','server','user'];
			foreach ($js_imports as $import) {
				echo '<script src="src/'.$import.'.js"></script>';
			}
		 ?>

		 <script src="testui.js"></script>

		<script>
		$(document).ready(function() {
			testUi.ready();
			SOE.ready();
		})
		</script>
	</head>
	<body>
		<form class="login-area">
			<p>
				Host:
				<select class="host-selection">
					<?php
						if ($host!=null) {
							echo '<option value="' . $host .'">Custom  ('.$host.')</option>';
						}
					 ?>
				</select>
			</p>
			<p>
				Username:
				<input class="username" required></input>
			</p>
			<p>
				Password:
				<input class="password" type="password" required></input>
			</p>
			<p>
				<button>Auth</button>
			</p>
		</form>

		<div class="profile start-hidden">
			<p>
				Welcome
				<span class='user-name'></span>.
				<span>
					<button class='logout'>Logout</button>
				</span>
				<br/>
				<i>Host:
					<span class='host'></span>
				</i>
			</p>


		</div>
		<div class="content-container start-hidden">

			<div class="content-tabs"></div>
			<div class="content"></div>
		</div>
		<p class="info"></p>

	</body>
</html>
