#!/bin/bash

loadUrl() {
  echo "Generating BASE URL"
  branch=$(git branch | sed -n -e 's/^\* \(.*\)/\1/p')
  case $branch in
    master)
      echo "soe.constant('BASE_URL', 'https://live.soe-app.org.uk/')" > www/js/env_var.js
	  ;;
    test)
	  echo "soe.constant('BASE_URL', 'http://soe-backend.sytes.net/')" > www/js/env_var.js
	  ;;
    develop)
      echo "soe.constant('BASE_URL', 'http://soe-backend.sytes.net/')" > www/js/env_var.js
	  ;;
    *)
      echo "soe.constant('BASE_URL', 'http://soe-backend.sytes.net/');" > www/js/env_var.js
      ;;
  esac
}

build-android() {
  echo "Generating BASE URL for SOE backend"
  loadUrl
  echo "Building APK"
  echo "============"
  cordova build --release android
  echo "============"
  echo "Signing APK"
  echo "============"
  jarsigner -verbose -sigalg SHA1withRSA\
    -digestalg SHA1 -keystore\
    Archive/keys/my-release-key.keystore\
    platforms/android/build/outputs/apk/android-release-unsigned.apk alias_name
  echo "============"
  echo "Optimising APK"
  echo "============"
  zipalign -v 4\
  platforms/android/build/outputs/apk/android-release-unsigned.apk\
  Archive/soe'_'`date +%s`.apk
  echo "============"
  echo "Build is successfully created. Find it here, Archive/soe_`date +%s`.apk"
}

build-ios() {
  echo "Generating BASE URL for SOE backend"
  loadUrl
  echo "Building App"
  echo "============"
  ionic build ios
  echo "Optimising App"
  cp -r platforms/ios\
    Archive/
    mv Archive/ios\
    Archive/soe'_'`date +%s`
  echo "============"
  echo "Build is successfully created. \
  Find it here, Archive/soe_`date +%s`"
}

run-web() {
  echo "Generating BASE URL for SOE backend"
  echo "soe.constant('BASE_URL', 'https://local.soe-app.org.uk/');" > www/js/env_var.js
  echo "Running SOE Web app"
  ionic serve -p 8101
}

run-ios() {
  loadUrl
  echo "Running on SOE app on iOS Device"
  ionic run ios
}

run-android() {
  loadUrl
  echo "Running on SOE app on Android Device"
  ionic run android
}

$1

