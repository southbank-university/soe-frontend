angular.module('soe.constants', [])

    .constant('NEWS', {
        account: 'lsbu_eng'
    })

    .constant('HOST', {
        url: 'https://soe-app.org.uk/'
    })

    .constant('STRINGS', {
        missingFields: 'Form is missing fields.',
        loggingIn: 'Logging you in...',
        loginError: 'There was an error logging you in.',
        unknownError: 'There was an error.'
    })
