soe.constant('soeData_URL', {
  GET_ALL_ITEM_URL: 'api/news-full?_format=json&page=',
  GET_ALL_ITEM_TYPE: 'GET',
  LOGIN_USER_URL: 'lsbu/soe?entity=user&type=login',
  LOGIN_USER_TYPE: 'POST',
  REGISTER_USER_URL: 'lsbu/soe?entity=user&type=register',
  REGISTER_USER_TYPE: 'POST',
  RESET_REQUEST_URL: 'lsbu/soe?entity=user&type=user_request_reset_pass',
  RESET_REQUEST_TYPE: 'POST',
  RESET_VERIFY_URL: 'lsbu/soe?entity=user&type=user_reset_pass',
  RESET_VERIFY_TYPE: 'POST',
  GET_ATTENDANCE_URL: 'api/lsbu_attendance?_format=json',
  GET_ATTENDANCE_TYPE: 'GET',
  GET_ABOUTPAGE_CONTENT_URL: 'node/4680/?_format=json',
  GET_ABOUTPAGE_CONTENT_TYPE: 'GET',
  GET_PRIVACYPOLICY_CONTENT_URL: 'node/4681/?_format=json',
  GET_PRIVACYPOLICY_TYPE: 'GET',
  GET_TANDC_CONTENT_URL: 'node/4682/?_format=json',
  GET_TANDC_CONTENT_TYPE: 'GET',
  GET_EULA_CONTENT_URL: 'node/4683/?_format=json',
  GET_EULA_CONTENT_TYPE: 'GET',
  POST_FEEDBACK_URL: 'lsbu/api/feedback',
  POST_FEEDBACK_TYPE: 'POST'
});

soe.constant('soeData_AUTH', {
  LOGIN_EMAIL: 'login_email',
  LOGIN_PASS: 'login_pass',
  REGISTER_NAME: 'set_name',
  REGISTER_EMAIL: 'set_email',
  REGISTER_PASS: 'set_pass',
  REGISTER_PASS_VALIDATE: 'set_pass2'
});

soe.constant('soeData_POSTITEM', {
  ITEM_NAME: 'name',
  ITEM_DESC: 'desc',
  ITEM_IMAGE: 'upImage'
});

soe.constant('soeData_REQUESTITEM', {
  USER_MESSAGE: 'user_message'
});

soe.constant('soeData_RESETPASS', {
  RESET_REQUEST: 'reset_email',
  RESET_PASS: 'reset_pass',
  RESET_PASS_VALIDATE: 'reset_pass2'
});
