soe.factory("Notificationfactory", function($cordovaLocalNotification) {
  return {
    /**
     * Scheduling notification.
     */
    add: function (id, message, title) {
      var alarmTime = new Date();
      $cordovaLocalNotification.schedule({
        id: id,
        date: alarmTime,
        message: message,
        title: title,
        autoCancel: true,
        sound: null
      }).then(function () {
        console.log("The notification has been set");
      });
    },
    /**
     * Check if notification is scheduled for a node.
     */
    isScheduled: function (id) {
    $cordovaLocalNotification.isScheduled(id).then(function(isScheduled) {
      console.log("Notification " + id + " Scheduled: " + isScheduled);
    });
    }
  }

});
