soe.controller('fullfeedctrl', function ($scope, $stateParams, $ionicTabsDelegate) {
  $ionicTabsDelegate.showBar(false);


  var feed_data = $stateParams.feed;
  $scope.title = feed_data['title'];
  $scope.body = feed_data['body'];
  $scope.date = feed_data['date'];
});