soe.controller('loginctrl',
  function($scope,
           $state,
           $ionicSideMenuDelegate,
           $ionicHistory,
           $stateParams,
           $localStorage,
           soeData_AUTH,
           soeData_URL,
           UserFactory,
           UIfactory,
           LoginNotifications,
           $ionicNavBarDelegate,
           $ionicTabsDelegate,
           BASE_URL) {

  UIfactory.hideSpinner();
  $scope.loginMessage = null;
  $ionicTabsDelegate.showBar(false);

  var EmailId = soeData_AUTH.LOGIN_EMAIL;
  var PassId = soeData_AUTH.LOGIN_PASS;

  $scope.loginUser = function () {
    UIfactory.showSpinner();
    var login_data = {
      email: inputVal.getValue(EmailId),
      password: inputVal.getValue(PassId)
    };
    var login = new UserFactory;
    login.loginCredentials(login_data.email, login_data.password);
    var cleanEmail = login.cleanEmail();
    if(login.validateEmail(cleanEmail) == true) {
      var loginFormSubmit = new Submitform('POST', BASE_URL + soeData_URL.LOGIN_USER_URL, JSON.stringify(login.loginFormData()), false);
      loginFormSubmit.ajaxSubmit(this)
    }
  };

  $scope.onSuccess = function (response) {
    if (response.status == 0) {
      UIfactory.hideSpinner();
      return UIfactory.showAlert('Alert', LoginNotifications.INVALID_ACCOUNT);
    }
    else if(response.status == 1) {
      switch (response.message) {
        case 'account not active':
          UIfactory.hideSpinner();
          UIfactory.showAlert('Alert', LoginNotifications.INACTIVE_ACCOUNT);
          break;
        case 'invalid password':
          UIfactory.hideSpinner();
          UIfactory.showAlert('Alert', LoginNotifications.INVALID_PASSWORD);
          break;
        case 'success login':
          UIfactory.hideSpinner();
          return userStorage(response.body);
        default:
          UIfactory.hideSpinner();
          return UIfactory.showAlert('Alert', LoginNotifications.ERROR_LOGIN);
      }
    }
  };

  $scope.onError = function (error) {
    UIfactory.hideSpinner();
    UIfactory.showAlert('Alert', LoginNotifications.ERROR_LOGIN);
  };

  var userStorage = function (data) {
    $localStorage.soe_user_status = 1;
    $localStorage.soe_user_username = data.username;
    $localStorage.soe_user_email = data.email;
    $localStorage.soe_user_token = data.uid;
    if(typeof $localStorage.soe_notification_status === 'undefined'){
      $localStorage.soe_notification_status = true;
    }
    $localStorage.expiry = new Date().getTime();
    return reloadForm();
  };

  var reloadForm = function() {
    inputVal.setValue(EmailId, '');
    inputVal.setValue(PassId, '');
    $ionicHistory.nextViewOptions({
       disableBack: true
    });
    return $state.go('tab.dash');
  };

  $scope.redirectUser = function (path) {
    UIfactory.showSpinner();
    if (path == 'register') {
      UIfactory.hideSpinner();
      $state.go('tab.register')
    }
    else if (path == 'forgot') {
      UIfactory.hideSpinner();
      $state.go('tab.resetrequest');
    }
  };



});
