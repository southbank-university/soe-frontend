soe.controller('activateaccountctrl', function($scope, $stateParams, $http, BASE_URL, soeData_URL) {

  var BASE_URL = 'http://live.soe-app.org.uk/lsbu/soe?entity=user&type=user_activate';
  var key = $stateParams.key;
  var uid = $stateParams.uid;
  $scope.status = 0;
  $scope.message = null;

  $http({
    url: BASE_URL,
    method: 'GET',
    params: {
      code: key,
      uid: uid
    }}).success(function(response) {
    $scope.status = response.status;
    $scope.message = response.message;
  }).error(function(err) {
    $scope.status = 0
  });
});
