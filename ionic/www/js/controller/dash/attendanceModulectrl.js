soe.controller('attendanceModulectrl', function($scope, $ionicTabsDelegate, $ionicHistory, $stateParams) {

  /**
   * Hide tab bar before view is created.
   */
  $scope.$on("$ionicView.beforeEnter", function () {
    $ionicTabsDelegate.showBar(false);
  });

  /**
   * @type {array}
   */
  var data = $stateParams.attendance;

  /**
   * @type {string}
   */
  $scope.module_name = data['moduleName'];

  /**
   * @type {array}
   */
  $scope.att = data['att'];

});