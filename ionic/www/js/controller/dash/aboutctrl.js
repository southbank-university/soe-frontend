soe.controller('aboutctrl', function($scope, $ionicTabsDelegate, soeData_URL, UIfactory, BASE_URL) {
	$ionicTabsDelegate.showBar(false);

  UIfactory.showSpinner();

  $scope.aboutContent = '';

	var aboutText = new Submitform(soeData_URL.GET_ABOUTPAGE_CONTENT_TYPE,
    BASE_URL + soeData_URL.GET_ABOUTPAGE_CONTENT_URL, undefined, false);

	aboutText.ajaxSubmit($scope);


	$scope.onSuccess = function (response) {
	  UIfactory.hideSpinner();

    for (var i = 0; i < response.body.length; i++) {
      $scope.aboutContent = response.body[i].value;
    }
	};

	$scope.onError = function (error) {
	  UIfactory.hideSpinner();
		console.log('error', error);
	};
});