soe.controller('settingsctrl', function ($scope, $ionicTabsDelegate, $ionicHistory, $state, $localStorage, $ionicNavBarDelegate, $stateParams) {
    $ionicNavBarDelegate.showBackButton(false);
  	$ionicHistory.clearCache();

  	$scope.username = $localStorage.soe_user_username;
  
	$scope.notificationHandler = function (event) {
		//make call to the backend
		console.log(event.target.checked);
		$localStorage.soe_notification_status = event.target.checked;
	};

	$scope.openAbout = function () {
		$state.go('tab.about');
	};

	$scope.openPrivacyPolicy = function () {
		$state.go('tab.privacypolicy');
	};

	$scope.openTermsAndConditions = function () {
		$state.go('tab.termsandconditions');
	};

  $scope.openEULA = function () {
    $state.go('tab.eula');
  };

	$scope.logoutUser = function () {
		$localStorage.soe_user_status = 0;
	    $localStorage.soe_user_username = null;
	    $localStorage.soe_user_email = null;
	    $localStorage.soe_user_token = null;
	    $localStorage.expiry = null;
	    $ionicHistory.nextViewOptions({disableBack: true});
		$state.go('tab.login', {reload: true, inherit: false});
	};

  /**
   * Custom navigation based on the state passed as route param.
   */
	$scope.goHome = function () {
	  switch ($stateParams.back_lock['page']) {
      case 'main':
        $state.go('tab.dash');
        break;
      case 'attendance':
        $state.go('tab.attendance');
        break;
      case 'feedback':
        $state.go('tab.feedback');
        break;
      default:
        $state.go('tab.dash');
    }
  };

});
