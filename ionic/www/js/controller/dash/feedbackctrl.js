soe.controller('feedbackctrl', function($scope, $ionicTabsDelegate, UserFactory, UIfactory, FeedbackFormNotification, BASE_URL, soeData_URL) {
	$scope.$on("$ionicView.beforeEnter", function () {
		$ionicTabsDelegate.showBar(true);
	});

	var userFactory = new UserFactory;

	//function sends the data to the backend
	$scope.submit = function () {
		UIfactory.showSpinner();
		var feedback = {
			subject: inputVal.getValue('feed_subject'),
			message: inputVal.getValue('feed_message')
		};
		
		if (userFactory.checkObjectForEmptyValues(feedback)) {
			userFactory.submit(JSON.stringify(feedback), BASE_URL + soeData_URL.POST_FEEDBACK_URL, $scope);
		} else {
			UIfactory.hideSpinner();
			UIfactory.showAlert('Alert', FeedbackFormNotification.MISSING_FIELD);
		}
	};

	$scope.onSuccess = function (response) {
		if(response = 0){
			UIfactory.hideSpinner();
			UIfactory.showAlert('Alert', FeedbackFormNotification.SENT_FAILURE);
		}else if(response = 1){
			UIfactory.hideSpinner();
			UIfactory.showAlert('Success', FeedbackFormNotification.SENT_SUCCESS);
		}
	};

	$scope.onError = function (response) {
		UIfactory.hideSpinner();
		console.log('something went wrong, data was not sent', response);
		UIfactory.showAlert('Alert', FeedbackFormNotification.SENT_FAILURE);
	}
});