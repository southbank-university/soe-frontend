soe.controller('DashCtrl',
  function($state,
           $ionicHistory,
           $scope,
           $http,
           $ionicPlatform,
           $location,
           $timeout,
           $localStorage,
           UIfactory,
           soeData_URL,
           $templateCache,
           $ionicScrollDelegate,
           $rootScope,
           $ionicTabsDelegate,
           Notificationfactory,
           BASE_URL) {

  $rootScope.slideHeader = false;
  $rootScope.pixelLimit = 0;
  
  /**
   * Display tab bar on view.
   */  
  $scope.$on("$ionicView.beforeEnter", function () {
    $ionicTabsDelegate.showBar(true);
  });

  /**
   * CHECK IF THIS IS REDUNTANT IF YES THEN BURN IT.
   */ 
  $scope.goForward = function () {
    var selected = $ionicTabsDelegate.selectedIndex();
    if (selected != 0) {
      $ionicTabsDelegate.select(selected + 1);
    }
  };

  /**
   * Loading spinner
   */
  UIfactory.showSpinner();

  /**
   * Home page view list & grid
   */
  $scope.viewType = 'list';
  $scope.changeToGrid = function(type) {
    if(type == 'list') {
      $scope.viewType = 'grid';
    }
    else if (type == 'grid') {
      $scope.viewType = 'list';
    }
  };

  /**
   * Variables and array to store and retrieve items
   */
  $scope.items = [];
  $scope.DOMFeeds = [];
  $scope.NewFeeds = [];
  var retrieved = 0;
  var page = 0;

  /**
   * Description: loadMore() function is used to retrieve items from the server.
   * Params: offset => last position of the items fetched,
   *         limit => number of items to be fetched (default = 10),
   *         filter => serach input
   * @return items from the server $scope.items[]
   */
  $scope.loadMore = function(callback) {
    $http({ url: BASE_URL + soeData_URL.GET_ALL_ITEM_URL + page,
            method: soeData_URL.GET_ALL_ITEM_TYPE}, {withCredentials: true}).success(function(response) {

      UIfactory.hideSpinner();

      // Navigate to call specific handler
      switch (callback) {
        case 'refresh':
          return handlePullToRefresh(response);
          break;
        case 'scroll':
          return handleInfiniteScroll(response);
        case 'initial':
          return handleInitialLoad(response);
          break;
        default:
          return true;

      }
    }).error(function(error) {
      $scope.loadMore();
    })
  };

  /**
   * Called on infinte scroll
   */
  $scope.loadInfiniteScroll = function () {
    page++;
    $scope.loadMore('scroll');
  };

  /**
   * Initial loadMore() call handler.
   */
  var handleInitialLoad = function(data) {
    for (var i = 0; i < data.length; i++) {
      $scope.DOMFeeds = $scope.DOMFeeds.concat(data[i]);
    }
  };

  /**
   * Executing pull to refresh.
   */
  $scope.pullToRefresh = function() {
    page = 0;
    $scope.loadMore('refresh');
  };

  /**
   * Description: check() function is called by infinite scroll to check if there
   * are items in the $scope.items array.
   */
  $scope.check = function() {
    return $scope.DOMFeeds.length > 0;
  };

  /**
   * Executing loadMore() with initial state when view is loaded.
   */
  $scope.$on('$ionicView.loaded', function() {
      $scope.loadMore('initial');
  });

  /**
   * Executing infinite scroll.
   */
  $scope.reloadData = function() {
    $state.go($state.current, {reload: true, inherit: false});
    $scope.$broadcast('scroll.refreshComplete');
  };

  /**
   * Broadcasting infinite scroll changes.
   */
  var handleInfiniteScroll = function (data) {
    for (var i = 0; i < data.length; i++) {
      $scope.DOMFeeds = $scope.DOMFeeds.concat(data[i]);
    }
    $scope.$broadcast('scroll.infiniteScrollComplete');
  };


  /**
   * Broadcasting pull to refresh changes.
   */
  var handlePullToRefresh = function (data) {
    pollingFeeds(data);
    $scope.$broadcast('scroll.refreshComplete');
  };

  /**
   * Polling feeds.
   *
   * It compare the new feeds with the ones already in viewport and add new ones (if any).
   * If the content type is News the it passes the data to notificationRequestLoader after appending to viewport.
   */
  var pollingFeeds = function (data) {
    for (var i = 0; i < data.length; i++) {
      var match = false;
      for (var j = 0; j < $scope.DOMFeeds.length; j++) {
        if (data[i]['nid'] === $scope.DOMFeeds[j]['nid']) {
          // Ignore if content is already in the DOM.
          match = true;
          break;
        }
      }
      if (!match) {
        // Add new content into DOM.
        $scope.NewFeeds = [];
        $scope.NewFeeds = $scope.NewFeeds.concat(data[i]);
        $scope.DOMFeeds = $scope.NewFeeds.concat($scope.DOMFeeds);

        if (data[i]['type'] == 'News') {
          notificationRequestLoader(data[i]);
        }

      }
    }
  };
    
  var notificationRequestLoader = function (req) {
    if (req['notify'] == 'Yes') {
      Notificationfactory.add(req['nid'], req['title'], req['subject']);
    }
  };

  $scope.fullView = function (feed_title, feed_body, feed_date) {
    $state.go('tab.fullfeed', {feed: {title: feed_title, body: feed_body, date: feed_date}});
  };

  /**
   * This is redirect the user to app.userguide state,
   * if the user has open the app for first time.
   */
  if($localStorage.app_launch_activity == 0) {
    UIfactory.showSpinner();
    $state.go('app.userguide');
  }
 });

soe.filter('htmlToPlaintext', function() {
  return function(text) {
    return  text ? String(text).replace(/<[^>]+>/gm, '') : '';
  };
});