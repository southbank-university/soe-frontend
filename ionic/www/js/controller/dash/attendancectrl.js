soe.controller('attendancectrl', function($scope, Chats, $ionicTabsDelegate, soeData_URL, $http, $templateCache, $location, UIfactory, $state, BASE_URL) {
  /**
   * Display tab bar on view.
   */
  $scope.$on("$ionicView.beforeEnter", function () {
    $ionicTabsDelegate.showBar(true);
  });

  /**
   * Display loading spinner.
   */
  UIfactory.showSpinner();

  /**
   * @type {Array}
   */
  $scope.attendance = [];

  /**
   * Call backend and retrieve attendance data.
   */
  $scope.loadAttendance = function() {
    $http({url: BASE_URL + soeData_URL.GET_ATTENDANCE_URL,
      method: soeData_URL.GET_ATTENDANCE_TYPE,
      cache: $templateCache}, {withCredentials: true}).success(function(response) {

      $scope.attendance = $scope.attendance.concat(response);
      prepareArray($scope.attendance);
      // Hide loading spinner.
      UIfactory.hideSpinner();
    }).error(function(error) {
      // Hide loading spinner.
      UIfactory.hideSpinner();
    });
  };
  /**
   * Execute $scope.loadAttendance() while view loading.
   */
  $scope.$on('$ionicView.loaded', function() {
    $scope.loadAttendance();
  });

  /**
   * Route to full attendance view state.
   *
   * @param module
   * @param att
   * @param day
   * @param time
   */
  $scope.fullView = function (module, att, room, time) {
    $state.go('tab.attendancemodule', {attendance: {moduleName: module, att: att, room: room, time:time}});
  };

  function prepareArray(arrayOfObjects) {
      //arrayOfObjects.reverse();
      arrayOfObjects.forEach(function(arrayObject) {
        arrayObject['day'] = dayFormat(arrayObject);
        arrayObject['time'] = timeFormat(arrayObject);
      });
  }

  /**
   * @returns {string}
   */
  function dayFormat(data) {
    switch (data['day']){
      case 'Mon':
        return 'Monday';
        break;
      case 'Tue':
        return 'Tuesday';
        break;
      case 'Wed':
        return 'Wednesday';
        break;
      case 'Thu':
        return 'Thursday';
        break;
      case 'Fri':
        return 'Friday';
        break;
      default:
        return null;
    }
  };

  function timeFormat(data) {
    var hour = data['time']
    var ampm = hour >= 12 ? 'pm' : 'am';
    hour = hour % 12;
    hour = hour ? hour : 12;
    var finalFormat = hour + ampm;
    return finalFormat;
  }

});
