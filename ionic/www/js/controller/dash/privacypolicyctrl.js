soe.controller('privacypolicyctrl', function($scope, $ionicTabsDelegate, soeData_URL, UIfactory, BASE_URL) {
  $ionicTabsDelegate.showBar(false);

  UIfactory.showSpinner();

  $scope.pageContent = '';

  var aboutText = new Submitform(soeData_URL.GET_PRIVACYPOLICY_TYPE,
    BASE_URL + soeData_URL.GET_PRIVACYPOLICY_CONTENT_URL, undefined, false);

  aboutText.ajaxSubmit($scope);


  $scope.onSuccess = function (response) {
    UIfactory.hideSpinner();

    for (var i = 0; i < response.body.length; i++) {
      $scope.pageContent = response.body[i].value;
    }
  };

  $scope.onError = function (error) {
    UIfactory.hideSpinner();
    console.log('error', error);
  };
});